#+title: i3-drip
#+author: Charged
#+PROPERTY: header-args :tangle install.sh
#+auto-tangle: t
#+STARTUP: showeverything
Installation script for i3gaps, with a Dracula theme!

* Table of contents :toc:
- [[#about-this-project][About this project]]
  - [[#working-distros][Working distros]]
- [[#credits][Credits]]
- [[#shebang--intro][Shebang + Intro]]
- [[#finding-distro][Finding distro]]
- [[#installing-packages][Installing packages]]
  - [[#arch-linux][Arch Linux]]
  - [[#debian][Debian]]
- [[#enable-lightdm][Enable LightDM]]
- [[#configuration][Configuration]]
  - [[#backup][Backup]]
  - [[#creating-folders-and-linking-configs][Creating folders and linking configs]]
  - [[#nitrogen-path-and-theme][Nitrogen path and theme]]
- [[#exit][Exit]]

* About this project
i3-drip is a [[https://gitlab.com/dtos][DTOS]] inspired project designed to make your window manager experience beautiful and easy! This script is written in Emacs Org mode. The way it works is just that it takes all the codeblocks from this file, then outputs it into a seperate file, which in this case is the script.

#+CAPTION: i3-drip Scrot
#+ATTR_HTML: :alt i3-drip Scrot :title i3-drip Scrot :align left
[[https://gitlab.com/chde1/i3-drip/-/raw/main/screenshots/noterminals.png]]

** Working distros
This script currently works with...
+ Arch Linux
+ Debian GNU/Linux

* Credits
Originally created by Charged,
Rewrite by @BenTheTechGuy

* Shebang + Intro
This it how all bash scripts start out, with a mighty shebang! We also need to make sure the script stops when there is an error.
#+begin_src sh
#!/bin/bash

# Intro
echo "Welcome to the i3-drip installer!"

# Stop script if an error occurs anywhere
set -e
#+end_src

* Finding distro
This script runs on 2 distros. To check which one the user is using, we need to take the output of /etc/os-release and check the distro.
#+begin_src sh
# Find distro
source /etc/os-release
# Used to find derivatives' base distro
if [ -v ID_LIKE ]; then
    ID=$ID_LIKE
fi
#+end_src

* Installing packages
We have 2 package managers used in this script, so we check for your distro, then use the correct one.

** Arch Linux
We're also building AUR packages here, with Git submodules.
#+begin_src sh
# Install packages
if [ "$ID" == "arch" ]; then
    # Install regular packages
    sudo pacman -S --needed base-devel xorg-server i3-gaps picom lightdm-slick-greeter lightdm-gtk-greeter-settings terminator ttf-font-awesome neofetch nitrogen starship i3lock feh lxappearance pulseaudio python-i3ipc

    # Initialize git submodules (AUR packages and Dracula theme)
    git submodule init
    git submodule update

    # Build AUR packages
    pushd autotiling > /dev/null
    makepkg -si --noconfirm
    popd > /dev/null

    pushd shell-color-scripts > /dev/null
    makepkg -si --noconfirm
    popd > /dev/null

    pushd bumblebee-status > /dev/null
    makepkg -si --noconfirm
    popd > /dev/null

    pushd nerd-fonts-hack > /dev/null
    makepkg -si --noconfirm
    popd > /dev/null
#+end_src


** Debian
We need to build i3-gaps from source, because it is not a Debian package yet.
#+begin_src sh
elif [ "$ID" == "debian" ]; then
    # Install regular packages
    sudo apt -y install make python3 python3-pip python3-i3ipc curl xserver-xorg terminator fonts-font-awesome fonts-hack neofetch nitrogen i3lock feh lxappearance picom lightdm pulseaudio-utils

    # Install i3-gaps
    git clone https://github.com/Airblader/i3.git i3-gaps

    # Install dependencies for i3-gaps compilation
    sudo apt -y install dh-autoreconf libxcb-keysyms1-dev libpango1.0-dev libxcb-util0-dev xcb libxcb1-dev libxcb-icccm4-dev \
    libyajl-dev libev-dev libxcb-xkb-dev libxcb-cursor-dev libxkbcommon-dev libxcb-xinerama0-dev libxkbcommon-x11-dev meson \
    ninja-build libstartup-notification0-dev libxcb-randr0-dev libxcb-xrm0 libxcb-xrm-dev libxcb-shape0 libxcb-shape0-dev

    mkdir -p i3-gaps/build
    pushd i3-gaps/build > /dev/null
    meson ..
    ninja
    sudo ninja install
    popd > /dev/null

    # Install autotiling
    sudo curl https://raw.githubusercontent.com/nwg-piotr/autotiling/v1.6/autotiling/main.py -o /usr/local/bin/autotiling
    sudo chmod 755 /usr/local/bin/autotiling

    # Install shell-color-scripts
    git clone https://gitlab.com/dwt1/shell-color-scripts.git /tmp/shell-color-scripts
    pushd /tmp/shell-color-scripts > /dev/null
    sudo make install
    popd > /dev/null

    # Install bumblebee-status
    pip3 install bumblebee-status

    # Install starship
    curl -L https://github.com/starship/starship/releases/download/v1.6.2/starship-x86_64-unknown-linux-gnu.tar.gz -o starship-x86_64-unknown-linux-gnu.tar.gz
    tar xf starship-x86_64-unknown-linux-gnu.tar.gz
    sudo mv starship /usr/local/bin

    # Dracula
    git submodule init wallpaper gtk
    git submodule update wallpaper gtk

    # Add i3 to lightdm
    echo "exec i3" > $HOME/.xsession
else
    echo "Your distribution is not supported."
    exit 1
fi
#+end_src

* Enable LightDM
LightDM is the display (login) manager i3-drip uses.
#+begin_src sh
sudo systemctl enable lightdm
#+end_src

* Configuration
This is where we add the theming and stuff. We create a backup for you, incase you hate i3-drip!

** Backup
We're your current configs to a backup folder, so you don't lose it.
#+begin_src sh
mkdir -p $HOME/.config/i3-drip/backup # Back up existing config files
# set up array of files in ~/.config to make code more compact and efficient
declare -a files=("picom.conf" "fish/config.fish" "nitrogen/bg-saved.cfg" "nitrogen/nitrogen.cfg" "gtk-3.0/settings.ini")
# loop through array and copy all the files to the backup folder
for file in "${files[@]}"; do if [ -e "$HOME/.config/$file" ]; then cp $HOME/.config/$file $HOME/.config/i3-drip/backup/$(basename $file); fi; done
# copy special config files manually
if [ -e "$HOME/.config/i3/config" ]; then cp $HOME/.config/i3/config $HOME/.config/i3-drip/backup/i3-config; fi
if [ -e "$HOME/.config/terminator/config" ]; then cp $HOME/.config/terminator/config $HOME/.config/i3-drip/backup/terminator-config; fi
if [ -e "$HOME/.bashrc" ]; then cp $HOME/.bashrc $HOME/.config/i3-drip/backup/bashrc; fi
if [ -e "/etc/lightdm/lightdm.conf" ]; then cp /etc/lightdm/lightdm.conf $HOME/.config/i3-drip/backup/lightdm.conf; fi
#+end_src

** Creating folders and linking configs
We symlink the configuration files to the actual destination, so everything is in one place.
#+begin_src sh
# Make config directories in case they're not already there
mkdir -p $HOME/.config/i3 $HOME/.config/nitrogen $HOME/.config/terminator $HOME/.config/gtk-3.0 $HOME/.config/fish $HOME/Pictures

# Apply configs
cp configs/* $HOME/.config/i3-drip
# Make symbolic links (shortcuts) from the real config locations to ~/.config/i3-drip
ln -sf $HOME/.config/i3-drip/picom.conf $HOME/.config/picom.conf
ln -sf $HOME/.config/i3-drip/config.fish $HOME/.config/fish/config.fish
ln -sf $HOME/.config/i3-drip/bg-saved.cfg $HOME/.config/nitrogen/bg-saved.cfg
ln -sf $HOME/.config/i3-drip/nitrogen.cfg $HOME/.config/nitrogen/nitrogen.cfg
ln -sf $HOME/.config/i3-drip/settings.ini $HOME/.config/gtk-3.0/settings.ini
ln -sf $HOME/.config/i3-drip/i3-config $HOME/.config/i3/config
ln -sf $HOME/.config/i3-drip/bashrc $HOME/.bashrc
sudo ln -sf $HOME/.config/i3-drip/lightdm.conf /etc/lightdm/lightdm.conf
#+end_src

** Nitrogen path and theme
Nitrogen needs you to manually set the home directory.
#+begin_src sh
# Replace ~ with /home/username in Nitrogen configs
sed -i "s|~|${HOME}|g" $HOME/.config/i3-drip/nitrogen.cfg
sed -i "s|~|${HOME}|g" $HOME/.config/i3-drip/bg-saved.cfg

# Apply dracula theme
mkdir -p $HOME/.themes
cp -r gtk $HOME/.themes/dracula
cp -r wallpaper $HOME/Pictures/dracula-wallpapers
#+end_src

* Exit
Letting the user know the script is over, and to reboot.
#+begin_src sh
# End
cat <<EOF
Installation complete!
A reboot is recommended for the new configuration to apply.
EOF
#+end_src
